from django.apps import AppConfig


class VisualizadorFacturasConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'visualizador_facturas'
