from django import forms

class LoginForm(forms.Form):
    numero_socio = forms.fields.CharField(label='Número de Socio', required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    contraseña = forms.fields.CharField(label='Contraseña', required=True, widget=forms.PasswordInput(attrs={'class': 'form-control'}))

