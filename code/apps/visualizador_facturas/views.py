from django.http.response import HttpResponseRedirect
from django.shortcuts import render

# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import FormView,ListView,View
from .forms import LoginForm
import os, datetime, locale

from django.templatetags.static import static


class LoginView(FormView):
    template_name = "visualizador_facturas/login.html"
    form_class = LoginForm
    form=form_class
    # success_url = reverse_lazy('/')

    def post(self, request, *args, **kwargs):
        form = self.form(request.POST)
        context = self.get_context_data(*args, **kwargs)
        if form.is_valid():
            numero_socio = self.request.POST.get('numero_socio')
            contraseña = self.request.POST.get('contraseña')

            if numero_socio == contraseña:
                request.session['numero_socio'] = numero_socio
                request.session['password'] = contraseña 
                return HttpResponseRedirect(f'/facturas/')  
            else:
                context['info'] = 'Contraseña o Usuario Incorrectos'
                return self.render_to_response(context=context)

class LogoutView(View): 
    def get(self,request):
        try:
            del self.request.session['numero_socio'] 
            del self.request.session['password'] 
        except Exception as e: 
            pass   
        return HttpResponseRedirect(reverse_lazy('login'))           

class FacturasPorSocio(ListView):
    template_name = "visualizador_facturas/facturas.html"
    context_object_name = 'informacion_cliente'

    def get_queryset(self):
        # num_socio = self.kwargs.get('slug')
        num_socio = self.request.session.get('numero_socio','None')
        # ruta_principal = r'/code/static/facturas'
        ruta_principal = os.path.abspath(os.getcwd()) + r'/static/facturas'
        listado_dirs = os.listdir(ruta_principal)
        listado_dirs.sort(reverse=True)
        informacion_cliente=[]
        posicion=0
        for dir in listado_dirs:
            año_mes = dir.split(' ') 
            ruta_secundaria = f'{ruta_principal}/{dir}'
            with os.scandir(ruta_secundaria) as ficheros:
                for factura in ficheros:
                    if num_socio in factura.name:
                        mes = año_mes[2]
                        informacion_cliente.extend([[año_mes[0],mes,dir,factura.name,posicion]])
                        posicion+=1
        return informacion_cliente
